from django.http.response import HttpResponsePermanentRedirect
from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def buyStock(request, ticker, amount):
    return HttpResponse(f'bought {amount} shares of {ticker}')

def sellStock(request, ticker, amount):
    return HttpResponse(f'sold {amount} shares of {ticker}')