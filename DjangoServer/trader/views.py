from django.http.response import HttpResponsePermanentRedirect
from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def buyStock(request, ticker, amount):
    return render(request, 'buyStock.html', {'ticker': ticker, 'amount': amount,})

def sellStock(request, ticker, amount):
    return render(request, 'sellStock.html', {'ticker': ticker, 'amount': amount,})